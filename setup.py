import setuptools
from __version import __VERSION__

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="adi-python123",
    version=__VERSION__,
    author="Adi Maf",
    author_email="bla@example.com",
    description="Python tests",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/amaft/python123",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
