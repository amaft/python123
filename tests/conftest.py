# sys path manipulation because of the way pytest finds root dir
import sys
import os

sys.path.append(os.path.join(sys.path[0], "python123"))
