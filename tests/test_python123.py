import python123.python123 as python123


def test_no_way():
    """Tests always one static method"""
    assert python123.no_way() is False
