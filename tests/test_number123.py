"""Tests for number package"""
from python123.number123.number123 import Number123


def test_is_number():
    """Tests is number function"""
    num1 = Number123(-4)
    assert num1.is_integer()


def test_always_one():
    """Tests always one static method"""
    assert Number123.always_one() == 1
