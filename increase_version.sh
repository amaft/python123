#!/usr/bin/env bash
CURRENT_VERSION=$(grep -ioP '[0-9]+\.[0-9]+\.[0-9]+([-.].*)?' __version.py)
if [[ ! -z "${CURRENT_VERSION}" ]]; then
    bump2version --commit --tag --current-version ${CURRENT_VERSION} patch __version.py
else
    echo "Could not read current version form __version.py"
    exit 1
fi