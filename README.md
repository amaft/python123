# python123

[![pipeline status](https://gitlab.com/amaft/python123/badges/master/pipeline.svg)](https://gitlab.com/amaft/python123/commits/master) [![coverage report](https://gitlab.com/amaft/python123/badges/master/coverage.svg)](https://gitlab.com/amaft/python123/commits/master)


just python things

Useful commands :
- 
- Package the app : python setup.py sdist bdist_wheel
- Upload to test repo : twine upload --repository-url https://test.pypi.org/legacy/ dist/*
- To use the module : pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple adi-python123