"""Number utils bla bla"""


class Number123:
    """Constructor with a number"""

    def __init__(self, value):
        self.value = value

    def is_integer(self):
        """Checks if integer"""
        return isinstance(self.value, int)

    @staticmethod
    def always_one():
        """Returns 1"""
        return 1
