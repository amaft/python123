"""I'm too lazy to write a docstring"""

from helper123 import helper123
from number123.number123 import Number123


def main():
    """Main entry point"""
    print("running python123")

    helper123.helper_function()
    num1 = Number123(3)
    yey = num1.is_integer()
    print(yey)
    return str(yey)


def no_way():
    """Will never do anything"""
    return False


if __name__ == "__main__":
    main()
