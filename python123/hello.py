"""Just some flask tests"""
import time
import datetime

from flask import Flask

import python123.python123
from python123.number123.number123 import Number123

APP = Flask(__name__)


@APP.route("/")
def hello_world():
    """Simple hello world"""
    return "Hello, World!"


@APP.route("/python123")
def python123_call():
    """Main function call"""
    return python123.python123.main()


@APP.route("/number123/isint/<int:number>")
def is_int_call(number):
    """Check if number is int"""
    is_it = Number123(number).is_integer()
    return str(is_it)


@APP.route("/long")
def long():
    """Just a long running test"""
    time.sleep(10)
    now = datetime.datetime.now()
    return str(now)
